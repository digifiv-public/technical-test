# Vue Technical Test



## Goal
You will develop a minimalistic Vue app with little requirements. Its goal is to test your knowledge and technical skill for the interview technical test.

- Use any Js Framework of your choice. (VueJs is Preferable)
- Use Composition API
- Use any UI component library of your choice.
- Use any Js Library of your choice.
- Use any UI Framework of your choice.


## Technical Test Requirements

Develop a poker game competition with include 2 to 6 players. Each round player will draw from a deck of card which contain 52 card. Player who has the largest card ranking will gain 1 winning point.
Upon each draw in the game round, display the player name who has the largest card ranking.
The competition ended when the deck of card is finished draw by players or there is not enough of card to be draw by the players in that game.
Upon ending of the competition, announce the players who have most winning points, and the players who have least winning points.

Game Rules
- The competition can be restart anytime after the competition is begin
- After game competition had begin or restart, the card deck should be shuffle, and shuffled card should not being reshuffle again throughout the entire game competition until it is end or restart.

## A step further (Extra Point)

Several actions could be triggerd from the web app:

- View game round's result throught out the game competition
- Allow players to have their own name when then game is start or restart
- Allow change of number of players when competition start
- And anything that seems relevant to you


## Submission

When the test is ready for submit, please ensure the following item:

- Web app can be build and run
- Send the web app project to hr or you may upload into any source code repository and provide the link to hr

## Discussion

When the test is finished and delivered, we will discuss:

- The tehcnological choices
- What was the challenge
- What could be improved
